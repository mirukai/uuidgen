# UUIDGen #

This is an application built in Python that is designed to generate a random UUID or GUID and output it to the console or to a file, or even batch generate them.

### What is this repository for? ###

* Creates GUIDs and UUIDs and outputs to stdoutput or to a file
* Version 0.6a-git

### How do I get set up? ###

* Download the source code, then simply run the `main.py` file with Python 3 or above.
* Dependencies: None, UUIDGen is a standalone program.

### Who do I talk to? ###

* [Hayden Young](mailto:hayden@haydenbjyoung.co.uk)